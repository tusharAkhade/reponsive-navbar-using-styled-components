import styled from "styled-components";
import Navbar from "./components/Navbar";

function App() {
  return (
    <Container>
      <Navbar />
    </Container>
  );
}

const Container = styled.div`
  background-color: #67bc98;
  height: 100vh;
`;

export default App;
